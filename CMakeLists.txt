cmake_minimum_required(VERSION 3.0)

project(LCM_Template VERSION 1.0 DESCRIPTION "A Template to get started with LCM" LANGUAGES C CXX)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/bin")

add_subdirectory(src)



