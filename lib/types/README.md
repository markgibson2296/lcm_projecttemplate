#H1 GIT SUBMODULES

LCM Types are managed by using the submodule feature in git:

Submodule are different git repositories under the same root.

This way you can manage 2 different project at folder level inside the root repository

Submodules allow foreign repositories to be embedded within a dedicated subdirectory of the source tree, always pointed at a particular commit.
git submodule

Break your big project to sub projects as you did so far.
Now add each sub project to you main project using :

git submodule add <url>
Once the project is added to your repo, you have to init and update it.

git submodule init
git submodule update

As of Git 1.8.2 new option --remote was adde

git submodule update --remote --merge

will fetch the latest changes from upstream in each submodule, merge them in, and check out the latest revision of the submodule.

source: https://stackoverflow.com/questions/36554810/how-to-link-folder-from-a-git-repo-to-another-repo