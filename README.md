**LCM Example Project**

Version 1.0 Initial Release
Author M.Gibson 10 October 2018

**Description:**

A test using Cmake to setup an LCM project, lcm types are found in:

root_dir/lib/lcm_types

As an initial test I have included example_t.lcm from the examples 
directory in LCM. To add additional messages to the project add the .lcm type to lcm_types directory,
add the message to the cmake file located in source (under lcm_wrap_types), finally add the correspondoing message header to the your source files. You do not have to use LCM gen using this project template. 

Currently only the listener from LCM examples is implmented, so to test I reccomened 
sending the example message from Matlab

**Install the following:**

* git
* cmake
* ninja 
* lcm v1.4.0
* An IDE or Text editor, I prefer Sublime Text 3 which is a feature rich text editor.

**Building from command line:**

1. Naviagte to the root directory of this project
2. Enter the following:

cmake -G Ninja

**Building from Sublime Text 3:**

1. Install Sublime text package manager (Google it). 
2. From command pallet, enter install package.
3. Install CMakeBuilder
4. Open the template.sublime-project
5. From the tools menu->CMakebuilder->Configure
6. Select the newly created template Build system
7. ctrl+b to build the project
8. the output is in the bin folder

Building from Visual Studio:
1. ????